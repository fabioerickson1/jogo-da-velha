import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Game from './src/screens/Game';

export default function App() {
  return (
    <View style={styles.container}>
      <Game /> 
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#dbdbdb',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
