import React, { Component } from "react";
import { View, Text, TouchableOpacity } from 'react-native';

class Game extends Component {
    constructor(props) {
        super(props);
        this.state = {
            actualPlayer: 1,
            gamePads: [null, null, null, null, null, null, null, null, null],
            hasVictory: false,
            player1: null,
            player2: null,
            endGame: false,
            message: ""
        }

        this.interval = null;

        this.botAtack = this.botAtack.bind(this);
        this.botDefense = this.botDefense.bind(this);
        this.botFirst = this.botFirst.bind(this);
        this.botRandom = this.botRandom.bind(this);
        this.handleChangePlayer = this.handleChangePlayer.bind(this);
        this.handlePadPress = this.handlePadPress.bind(this);
        this.resetGame = this.resetGame.bind(this);
        this.handleTurn = this.handleTurn.bind(this);
        this.validatePossibility = this.validatePossibility.bind(this);
        this.validateVictory = this.validateVictory.bind(this);
    }

    botAtack() {
        this.setState({ message: "Tentando atacar..." });
        const { gamePads, actualPlayer } = this.state;
        const newGamePads = [...gamePads];

        const index = this.validatePossibility(actualPlayer, "ATAQUE");
        if (index === false) {
            this.botRandom();
            return;
        }
        newGamePads[index] = actualPlayer;
        this.setState({ gamePads: newGamePads }, () => {
            this.handleChangePlayer();
        })
    }

    botDefense() {
        this.setState({ message: "Tentando defender..." });
        const { gamePads, actualPlayer } = this.state;
        const newGamePads = [...gamePads];

        const oponent = actualPlayer === 1 ? 2 : 1;

        const index = this.validatePossibility(oponent, "DEFESA");
        if (index === false) {
            this.botRandom();
            return;
        }
        newGamePads[index] = actualPlayer;
        this.setState({ gamePads: newGamePads }, () => {
            this.handleChangePlayer();
        })
    }

    botFirst() {
        this.setState({ message: "Jogando na primeira casa vazia..." });
        const { gamePads, actualPlayer } = this.state;
        const newGamePads = [...gamePads];
        let done = false;
        newGamePads.forEach((actualPad, index) => {
            if (actualPad === null && !done) {
                newGamePads[index] = actualPlayer;
                done = true;
            }
        });
        this.setState({ gamePads: newGamePads }, () => {
            this.handleChangePlayer();
        });
    }

    botRandom() {
        this.setState({ message: "Jogando aleatoriamente..." });
        const { gamePads, actualPlayer } = this.state;
        const newGamePads = [...gamePads];
        while (true) {
            const random = Math.random() * (8 - 0) + 0;
            const index = random.toFixed(0);
            if (newGamePads[index] === null) {
                newGamePads[index] = actualPlayer;
                this.setState({ gamePads: newGamePads }, () => {
                    this.handleChangePlayer();
                });
                return;
            }
            if (newGamePads.find(item => item === null) === undefined) return;
        }
    }

    handleChangePlayer() {
        const { actualPlayer } = this.state;
        const hasVictory = this.validateVictory();
        if (hasVictory) return;
        if (actualPlayer === 1) {
            this.setState({ actualPlayer: 2 });
            return;
        }
        this.setState({ actualPlayer: 1 });
    }

    handlePadPress(index) {
        this.setState({ message: "Jogada humana..." });
        const { gamePads, actualPlayer } = this.state;

        const newGamePads = [...gamePads];
        if (newGamePads[index] !== null) return;
        newGamePads[index] = actualPlayer;
        this.setState({ gamePads: newGamePads }, () => {
            this.handleChangePlayer();
        })
    }

    resetGame() {
        this.setState({
            gamePads: [null, null, null, null, null, null, null, null, null],
            actualPlayer: 1,
            player1: null,
            player2: null,
            endGame: false,
            hasVictory: false,
            message: ""
        });
        clearInterval(this.interval);
    }

    handleTurn() {
        const { player1, player2, actualPlayer, gamePads } = this.state;
        if (gamePads.find(item => item === null) === undefined) this.setState({ endGame: true }, () => {
            clearInterval(this.interval);
            return;
        });
        const selectedBot = actualPlayer === 1 ? player1 : player2;
        const player = actualPlayer === 1 ? "X" : "O";
        console.log("VEZ DE: " + player);
        switch (selectedBot) {
            case 1:
                this.botFirst();
                break;
            case 2:
                this.botRandom();
                break;
            case 3:
                this.botAtack();
                break;
            case 4:
                this.botDefense();
                break;
            default:
                break;
        }
    }

    startGame() {
        const { player1, player2, hasVictory } = this.state;
        if (player1 === null) return;
        if (player2 === null) return;
        if (hasVictory) {
            clearInterval(this.interval);
            return;
        };
        this.handleTurn();
        this.interval = setInterval(this.handleTurn, 1500);
    }

    validatePossibility(player, type) {
        const { gamePads } = this.state;
        console.log("TENTANDO " + type);
        // Primeira linha
        if (gamePads[0] === player) {
            if (gamePads[1] === player) {
                if (gamePads[2] === null) {
                    console.log("PRIMEIRA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 2 " + type);
                    return 2;
                }
            } else if (gamePads[2] === player) {
                if (gamePads[1] === null) {
                    console.log("PRIMEIRA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 1 " + type);
                    return 1;
                }
            }
        } else if (gamePads[1] === player) {
            if (gamePads[2] === player) {
                if (gamePads[0] === null) {
                    console.log("PRIMEIRA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 0 " + type);
                    return 0;
                }
            }
        }
        // Segunda linha
        if (gamePads[3] === player) {
            if (gamePads[4] === player) {
                if (gamePads[5] === null) {
                    console.log("SEGUNDA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 5 " + type);
                    return 5;
                }
            } else if (gamePads[5] === player) {
                if (gamePads[4] === null) {
                    console.log("SEGUNDA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 4 " + type);
                    return 4;
                }
            }
        } else if (gamePads[4] === player) {
            if (gamePads[5] === player) {
                if (gamePads[3] === null) {
                    console.log("SEGUNDA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 3 " + type);
                    return 3;
                }
            }
        }
        // Terceira linha
        if (gamePads[6] === player) {
            if (gamePads[7] === player) {
                if (gamePads[8] === null) {
                    console.log("TERCEIRA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 8 " + type);
                    return 8;
                }
            } else if (gamePads[8] === player) {
                if (gamePads[7] === null) {
                    console.log("TERCEIRA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 7 " + type);
                    return 7;
                }
            }
        } else if (gamePads[7] === player) {
            if (gamePads[8] === player) {
                if (gamePads[6] === null) {
                    console.log("TERCEIRA LINHA");
                    console.log("POSSIVEL JOGADA NA CASA 6 " + type);
                    return 6;
                }
            }
        }
        // Primeira coluna
        if (gamePads[0] === player) {
            if (gamePads[3] === player) {
                if (gamePads[6] === null) {
                    console.log("PRIMEIRA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 6 " + type);
                    return 6;
                }
            } else if (gamePads[6] === player) {
                if (gamePads[3] === null) {
                    console.log("PRIMEIRA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 3 " + type);
                    return 3;
                }
            }
        } else if (gamePads[3] === player) {
            if (gamePads[6] === player) {
                if (gamePads[0] === null) {
                    console.log("PRIMEIRA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 0 " + type);
                    return 0;
                }
            }
        }
        // Segunda coluna
        if (gamePads[1] === player) {
            if (gamePads[4] === player) {
                if (gamePads[7] === null) {
                    console.log("SEGUNDA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 7 " + type);
                    return 7;
                }
            } else if (gamePads[7] === player) {
                if (gamePads[4] === null) {
                    console.log("SEGUNDA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 4 " + type);
                    return 4;
                }
            }
        } else if (gamePads[4] === player) {
            if (gamePads[7] === player) {
                if (gamePads[1] === null) {
                    console.log("SEGUNDA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 1 " + type);
                    return 1;
                }
            }
        }
        // Terceira coluna
        if (gamePads[2] === player) {
            if (gamePads[5] === player) {
                if (gamePads[8] === null) {
                    console.log("TERCEIRA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 8 " + type);
                    return 8;
                }
            } else if (gamePads[8] === player) {
                if (gamePads[5] === null) {
                    console.log("TERCEIRA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 5 " + type);
                    return 5;
                }
            }
        } else if (gamePads[5] === player) {
            if (gamePads[8] === player) {
                if (gamePads[2] === null) {
                    console.log("TERCEIRA COLUNA");
                    console.log("POSSIVEL JOGADA NA CASA 2 " + type);
                    return 2;
                }
            }
        }
        // Primeira diagonal
        if (gamePads[0] === player) {
            if (gamePads[4] === player) {
                if (gamePads[8] === null) {
                    console.log("PRIMEIRA DIAGONAL");
                    console.log("POSSIVEL JOGADA NA CASA 8 " + type);
                    return 8;
                }
            } else if (gamePads[8] === player) {
                if (gamePads[4] === null) {
                    console.log("PRIMEIRA DIAGONAL");
                    console.log("POSSIVEL JOGADA NA CASA 4 " + type);
                    return 4;
                }
            }
        } else if (gamePads[4] === player) {
            if (gamePads[8] === player) {
                if (gamePads[0] === null) {
                    console.log("PRIMEIRA DIAGONAL");
                    console.log("POSSIVEL JOGADA NA CASA 0 " + type);
                    return 0;
                }
            }
        }
        // Segunda diagonal
        if (gamePads[2] === player) {
            if (gamePads[4] === player) {
                if (gamePads[6] === null) {
                    console.log("SEGUNDA DIAGONAL");
                    console.log("POSSIVEL JOGADA NA CASA 6 " + type);
                    return 6;
                }
            } else if (gamePads[6] === player) {
                if (gamePads[4] === null) {
                    console.log("SEGUNDA DIAGONAL");
                    console.log("POSSIVEL JOGADA NA CASA 4 " + type);
                    return 4;
                }
            }
        } else if (gamePads[4] === player) {
            if (gamePads[6] === player) {
                if (gamePads[2] === null) {
                    console.log("SEGUNDA DIAGONAL");
                    console.log("POSSIVEL JOGADA NA CASA 2 " + type);
                    return 2;
                }
            }
        }
        return false;
    }

    validateVictory() {
        const { gamePads } = this.state;
        if (gamePads.find(item => item === null) === undefined) this.setState({ endGame: true }, () => {
            clearInterval(this.interval);
            return;
        });
        // Primeira linha
        if (gamePads[0] === gamePads[1] && gamePads[0] === gamePads[2] && gamePads[0] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        // Segunda linha
        if (gamePads[3] === gamePads[4] && gamePads[3] === gamePads[5] && gamePads[3] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        // Terceira linha
        if (gamePads[6] === gamePads[7] && gamePads[6] === gamePads[8] && gamePads[6] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        // Primeira coluna
        if (gamePads[0] === gamePads[3] && gamePads[0] === gamePads[6] && gamePads[0] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        // Segunda coluna
        if (gamePads[1] === gamePads[4] && gamePads[1] === gamePads[7] && gamePads[1] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        // Terceira coluna
        if (gamePads[2] === gamePads[5] && gamePads[2] === gamePads[8] && gamePads[2] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        // Primeira diagonal
        if (gamePads[0] === gamePads[4] && gamePads[0] === gamePads[8] && gamePads[0] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        // Segunda diagonal
        if (gamePads[2] === gamePads[4] && gamePads[2] === gamePads[6] && gamePads[2] !== null) {
            this.setState({ hasVictory: true });
            clearInterval(this.interval);
            return true;
        }
        return false;
    }

    render() {
        const { actualPlayer, gamePads, hasVictory, player1, player2, endGame, message } = this.state;

        renderPadStyle = (player) => {
            if (player === 1) return <Text style={{ fontSize: 60, textAlign: "center", color: "#00ff00" }}>X</Text>;
            else return <Text style={{ fontSize: 60, textAlign: "center", color: "#ff0000" }}>O</Text>;
        }

        return (
            <View>
                <View style={{ backgroundColor: "#1063b7", borderRadius: 5, margin: 10, marginTop: 30, padding: 5 }}>
                    {!hasVictory ?
                        !endGame ?
                            <View>
                                <Text style={{ fontSize: 20, textAlign: "center", color: "#fff" }}>Bots</Text>
                                <View style={{ display: "flex", alignItems: "center", flexDirection: "row", justifyContent: "space-between" }}>
                                    <Text style={{ fontSize: 20, textAlign: "center", color: "#fff" }}>{renderPadStyle(1)}</Text>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player1: 1 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player1 === 1 ? "#00ff00" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Prim</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player1: 2 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player1 === 2 ? "#00ff00" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Alea</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player1: 3 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player1 === 3 ? "#00ff00" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Atk</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player1: 4 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player1 === 4 ? "#00ff00" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Def</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ display: "flex", alignItems: "center", flexDirection: "row", justifyContent: "space-between" }}>
                                    <Text style={{ fontSize: 20, textAlign: "center", color: "#fff" }}>{renderPadStyle(2)}</Text>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player2: 1 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player2 === 1 ? "#ff0000" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Prim</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player2: 2 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player2 === 2 ? "#ff0000" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Alea</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player2: 3 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player2 === 3 ? "#ff0000" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Atk</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.setState({ player2: 4 })}>
                                        <View style={{ width: 50, height: 50, backgroundColor: player2 === 4 ? "#ff0000" : "#fff", borderRadius: 5, justifyContent: "center" }}>
                                            <Text style={{ fontSize: 15, textAlign: "center", color: "#000" }}>Def</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            :
                            <View>
                                <Text style={{ fontSize: 20, textAlign: "center", color: "#fff" }}>Deu velha!</Text>
                                <View style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                                    <Text style={{ fontSize: 20, textAlign: "center", color: "#fff" }}>Nenhum jogador venceu!</Text>
                                </View>
                            </View>
                        :
                        <View>
                            <Text style={{ fontSize: 20, textAlign: "center", color: "#fff" }}>O jogo acabou!</Text>
                            <View style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                                {renderPadStyle(actualPlayer)}
                                <Text style={{ fontSize: 20, textAlign: "center", color: "#fff" }}> ganhou!</Text>
                            </View>
                        </View>
                    }
                </View>
                <View style={{ display: "flex", flexDirection: "row", flexWrap: "wrap", justifyContent: "center", alignItems: "center", backgroundColor: "#1173B7", width: "100%" }}>
                    <View style={{ maxWidth: "90%", display: "flex", flexDirection: "row", flexWrap: "wrap", justifyContent: "center", alignItems: "center" }}>
                        {gamePads.map((actualPad, index) => (
                            <TouchableOpacity disabled={hasVictory} key={index} onPress={() => this.handlePadPress(index)}>
                                <View style={{ borderRadius: 5, backgroundColor: "#fff", height: 80, width: 80, margin: 10 }} >
                                    {actualPad !== null ? renderPadStyle(actualPad) : null}
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>
                <View style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                    {!hasVictory && !endGame &&
                        <TouchableOpacity style={{ padding: 10 }} onPress={() => this.startGame()}>
                            <View style={{ height: 50, backgroundColor: "#1173B7", borderRadius: 5, paddingLeft: 10, paddingRight: 10 }}>
                                <Text style={{ fontSize: 25, textAlign: "center", marginTop: 10, color: "#fff" }}>{gamePads.find(item => item !== null) !== undefined ? "Próximo" : "Começar"}</Text>
                            </View>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity style={{ padding: 10 }} onPress={() => this.resetGame()}>
                        <View style={{ height: 50, backgroundColor: "#fff", borderColor: "#1173b7", borderWidth: 1, borderRadius: 5, paddingLeft: 10, paddingRight: 10 }}>
                            <Text style={{ fontSize: 25, textAlign: "center", color: "#1173B7", marginTop: 10 }}>Resetar</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ backgroundColor: "#fff", borderColor: "#1173b7", borderWidth: 1, borderRadius: 5, paddingLeft: 10, paddingRight: 10 }}>
                    <Text style={{ fontSize: 20, textAlign: "center", color: "#1173B7", margin: 10 }}>{message}</Text>
                </View>
            </View>
        )
    }
}

export default Game;